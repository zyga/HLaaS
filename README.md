<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# Lab as a Service space

This is Lab as a Service space for building Oniroproject related lab. The lab consists of several areas:

More information can be found at: https://docs.oniroproject.org/en/latest/

* Oniroproject
* Oniroproject blueprints
* Infrastructure as Code (IaC) for installation of software
* Lava
* Hardware level

## WRC Lab shelves

Directory 3D_shelves contain rack shelves with trays for different devices under test (DUT)

Calculation of shelves needed:

1. 6x RPi4 + disks = 2 shelves
1. ykush switches x2, SBC C61 = 1 shelf
1. SBC B68, 2x relays, 2x nitrogens, 2x arduinos = 2+2+2+2 = 8 = 1.5 shelves

Total ~ 5 shelves

## Contributing

See the `CONTRIBUTING.md` file.

## License

The license of this repository is as follows:

* Documentation text is under `CC-BY-4.0` license
* 3D printed designs of shelves and trays under `CC-BY-NC-SA-4.0` license
* Scripts, tools, and so on, are under `Apache-2.0` license

See the `LICENSES` subdirectory for full license texts.

